// ==UserScript==
// @name         OGame Auction Hotkeys
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  Повысить ставку: q - металл, w - кристалл, e - дейтерий. Сделать ставку - r. Raise the bet: q - metal, w - crystal, e - deuterium. Make a bet - r.
// @author       Deimos
// @match        https://*.ogame.gameforge.com/game/index.php*component=traderOverview*
// @grant        none
// @license      MIT
// ==/UserScript==

(function() {
    'use strict';
    document.onkeydown = function(e) {
        if (e.ctrlKey || e.altKey || e.metaKey) return;
        var char = e.which;
        var metal = parseInt(document.getElementsByClassName("js_sliderMetalInput")[0].value.replace('.',''), 10);
        if (!char) return;
        switch (char) {
            case 65:
                document.querySelector("a.js_sliderMetalMax").click();
                break;
            case 90:
                document.querySelector("a.js_sliderCrystalMax").click();
                break;
            case 69:
                document.querySelector("a.js_sliderDeuteriumMax").click();
                break;
            case 82:
                document.querySelector("a.pay").click();
                break;
            case 81:
                document.querySelector("a.js_sliderMetalMax").click();
                document.querySelector("a.pay").click();
                break;
            case 83:
                document.querySelector("a.js_sliderCrystalMax").click();
                document.querySelector("a.pay").click();
                break;
            case 68:
                document.querySelector("a.js_sliderDeuteriumMax").click();
                document.querySelector("a.pay").click();
                break;
            case 87:
                metal = metal + 10000;
                document.getElementsByClassName("js_sliderMetalInput")[0].value=metal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                document.querySelector("a.js_sliderMetalMore").click();
                document.querySelector("a.pay").click();
                break;
            case 88:
                metal = metal + 100000;
                document.getElementsByClassName("js_sliderMetalInput")[0].value=metal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                document.querySelector("a.js_sliderMetalMore").click();
                document.querySelector("a.pay").click();
                break;
            case 67:
                metal = metal + 1000000;
                document.getElementsByClassName("js_sliderMetalInput")[0].value=metal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                document.querySelector("a.js_sliderMetalMore").click();
                document.querySelector("a.pay").click();
                break;
            case 86:
                metal = metal + 2000000;
                document.getElementsByClassName("js_sliderMetalInput")[0].value=metal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                document.querySelector("a.js_sliderMetalMore").click();
                document.querySelector("a.pay").click();
                break;
            case 66:
                metal = metal + 3000000;
                document.getElementsByClassName("js_sliderMetalInput")[0].value=metal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                document.querySelector("a.js_sliderMetalMore").click();
                document.querySelector("a.pay").click();
                break;

        }
        return;
    };
})();
